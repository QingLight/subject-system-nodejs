USE [DB_TeachingMS]
GO
/****** Object:  User [Student]    Script Date: 2022/5/25 12:07:08 ******/
CREATE USER [Student] FOR LOGIN [Student] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Teacher]    Script Date: 2022/5/25 12:07:08 ******/
CREATE USER [Teacher] FOR LOGIN [DESKTOP-PUTMSHO\Teacher] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [Teachers]    Script Date: 2022/5/25 12:07:08 ******/
CREATE ROLE [Teachers]
GO
ALTER ROLE [Teachers] ADD MEMBER [Teacher]
GO
/****** Object:  Schema [Teachers]    Script Date: 2022/5/25 12:07:08 ******/
CREATE SCHEMA [Teachers]
GO
/****** Object:  UserDefinedFunction [dbo].[GET_SCORE]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GET_SCORE]
( @stu_name char(8) )
RETURNS int
AS
BEGIN
 DECLARE @sum int
 SELECT @sum = sum(d.CourseGrade) from 
	(select c.CourseGrade from TB_Student a,TB_Grade b,TB_Course c 
	WHERE a.StuName = @stu_name and a.StuID = b.StuID and b.TotalScore >= 60 and b.CourseID = c.CourseID) d
 RETURN @sum
END

GO
/****** Object:  Table [dbo].[TB_Class]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Class](
	[ClassID] [char](6) NOT NULL,
	[ClassName] [char](20) NOT NULL,
	[DeptID] [char](2) NOT NULL,
	[TeacherID] [char](6) NOT NULL,
	[ClassNumber] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Course]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Course](
	[CourseID] [char](6) NOT NULL,
	[CourseName] [varchar](32) NOT NULL,
	[DeptID] [char](2) NOT NULL,
	[CourseGrade] [real] NOT NULL,
	[LessonTime] [smallint] NOT NULL,
	[CourseOutline] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_CourseClass]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_CourseClass](
	[CourseClassID] [char](10) NOT NULL,
	[CourseID] [char](6) NOT NULL,
	[TeacherID] [char](6) NOT NULL,
	[TeachingYearID] [char](4) NOT NULL,
	[TermID] [char](2) NOT NULL,
	[TeachingPlace] [nvarchar](16) NOT NULL,
	[TeachingTime] [nvarchar](32) NOT NULL,
	[CommonPart] [tinyint] NOT NULL,
	[MiddlePart] [tinyint] NOT NULL,
	[LastPart] [tinyint] NOT NULL,
	[MaxNumber] [smallint] NOT NULL,
	[SelectedNumber] [smallint] NOT NULL,
	[FullFlag] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Dept]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Dept](
	[DeptID] [char](2) NOT NULL,
	[DeptName] [char](20) NOT NULL,
	[DeptSetDate] [smalldatetime] NOT NULL,
	[DeptScript] [text] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DeptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Grade]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Grade](
	[GradeSeedID] [int] IDENTITY(1,1) NOT NULL,
	[StuID] [char](8) NOT NULL,
	[ClassID] [char](6) NOT NULL,
	[CourseClassID] [char](10) NOT NULL,
	[CourseID] [char](6) NOT NULL,
	[CommonScore] [real] NOT NULL,
	[MiddleScore] [real] NOT NULL,
	[LastScore] [real] NOT NULL,
	[TotalScore] [real] NOT NULL,
	[RetestScore] [real] NULL,
	[LockFlag] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[GradeSeedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_SelectCourse]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_SelectCourse](
	[StuID] [char](8) NOT NULL,
	[CourseClassID] [char](10) NOT NULL,
	[SelectDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_StuID_CourseClassID] PRIMARY KEY CLUSTERED 
(
	[StuID] ASC,
	[CourseClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Spec]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Spec](
	[SpecID] [char](4) NOT NULL,
	[SpecName] [char](20) NOT NULL,
	[DeptID] [char](2) NOT NULL,
	[SpecScript] [text] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SpecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Student]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Student](
	[StuID] [char](8) NOT NULL,
	[StuName] [char](8) NOT NULL,
	[EnrollYear] [char](4) NOT NULL,
	[GradYear] [char](4) NOT NULL,
	[DeptID] [char](2) NOT NULL,
	[ClassID] [char](6) NOT NULL,
	[Sex] [char](1) NOT NULL,
	[Birthday] [smalldatetime] NOT NULL,
	[SPassword] [varchar](32) NOT NULL,
	[StuAddress] [varchar](64) NOT NULL,
	[ZipCode] [char](6) NOT NULL,
	[TotalGrade] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[StuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Teacher]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Teacher](
	[TeacherID] [char](6) NOT NULL,
	[TeacherName] [char](8) NOT NULL,
	[DeptID] [char](2) NOT NULL,
	[Sex] [char](1) NOT NULL,
	[Birthday] [smalldatetime] NOT NULL,
	[TPassword] [varchar](32) NOT NULL,
	[TitleID] [char](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TeacherID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_TeachingYear]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_TeachingYear](
	[TeachingYearID] [char](4) NOT NULL,
	[TeachingYearName] [nchar](11) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TeachingYearID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Term]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Term](
	[TermID] [char](2) NOT NULL,
	[TermName] [char](8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TermID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_Title]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_Title](
	[TitleID] [char](2) NOT NULL,
	[TitleName] [char](8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TitleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'040198', N'软件工程190         ', N'02', N'T02001', NULL)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'040199', N'软件工程191         ', N'02', N'T08004', NULL)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'040200', N'软件工程191         ', N'02', N'T08004', NULL)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'040201', N'04机电(1)班         ', N'02', N'T02001', 13)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'040801', N'04网络(1)班         ', N'08', N'T08001', 10)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'040802', N'04网络(2)班         ', N'08', N'T08002', 10)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'050302', N'05电子(2)班         ', N'03', N'T10005', 10)
INSERT [dbo].[TB_Class] ([ClassID], [ClassName], [DeptID], [TeacherID], [ClassNumber]) VALUES (N'050801', N'05软件(1)班         ', N'08', N'T08003', 12)
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C07000', N'Ab数据库应用技术', N'07', 2, 54, N'数据库技术 SQL Server')
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C07001', N'中国剪纸艺术', N'07', 2, 36, N'传统艺术')
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C08002', N'C语言程序设计', N'08', 3, 54, NULL)
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C08003', N'Flash动画制作', N'08', 3, 54, N'简单动画设计与制作')
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C08004', N'动态网页设计', N'08', 2, 32, N'构架一个自己的网站')
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C10001', N'曹雪芹与红楼梦', N'10', 2, 36, NULL)
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C10002', N'歌唱艺术与欣赏', N'10', 2, 32, NULL)
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C10004', N'吉他弹唱', N'10', 2, 32, NULL)
INSERT [dbo].[TB_Course] ([CourseID], [CourseName], [DeptID], [CourseGrade], [LessonTime], [CourseOutline]) VALUES (N'C10005', N'数学思想与方法', N'10', 2, 36, NULL)
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T070020399', N'C07001', N'T07002', N'2004', N'T1', N'杭师大勤7-403', N'3:3-4,5:3-4', 40, 20, 40, 80, 1, N'U')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T070020401', N'C07001', N'T07002', N'2004', N'T1', N'4#102', N'3:3-4,5:3-4', 40, 0, 60, 10, 10, N'F')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T080010401', N'C08002', N'T08001', N'2004', N'T1', N'4#多媒体208', N'1:3-4,3:1-2', 10, 20, 70, 10, 10, N'F')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T080010402', N'C08002', N'T08001', N'2004', N'T1', N'4#普通教室208', N'2:3-4,4:5-6', 10, 20, 70, 10, 10, N'F')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T080030401', N'C08004', N'T08003', N'2004', N'T2', N'4#录播室304', N'1:1-2,3:3-4', 60, 0, 40, 10, 10, N'F')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T080040401', N'C08003', N'T08004', N'2004', N'T2', N'8#309', N'2:3-4,4:5-6', 60, 0, 40, 8, 6, N'U')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T100020401', N'C10004', N'T10002', N'2004', N'T1', N'10#208', N'1:3-4,3:1-2', 50, 0, 50, 5, 1, N'U')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T100020402', N'C10004', N'T10002', N'2004', N'T1', N'4#多媒体304', N'2:7-8,4:7-8', 50, 0, 50, 10, 1, N'U')
INSERT [dbo].[TB_CourseClass] ([CourseClassID], [CourseID], [TeacherID], [TeachingYearID], [TermID], [TeachingPlace], [TeachingTime], [CommonPart], [MiddlePart], [LastPart], [MaxNumber], [SelectedNumber], [FullFlag]) VALUES (N'T100050401', N'C10001', N'T10005', N'2004', N'T1', N'10#202', N'1:3-4,3:1-2', 100, 0, 0, 5, 4, N'U')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'01', N'软件工程系          ', CAST(N'2022-01-01T00:00:00' AS SmallDateTime), N'略略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'02', N'机电工程系          ', CAST(N'1978-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'03', N'电子工程系          ', CAST(N'1978-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'05', N'化纺工程系          ', CAST(N'1978-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'06', N'外语系              ', CAST(N'1998-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'07', N'艺术设计系          ', CAST(N'1984-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'08', N'计算机系            ', CAST(N'2002-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'09', N'管理系              ', CAST(N'1978-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'10', N'基础部              ', CAST(N'1978-01-01T00:00:00' AS SmallDateTime), N'略')
INSERT [dbo].[TB_Dept] ([DeptID], [DeptName], [DeptSetDate], [DeptScript]) VALUES (N'11', N'体育部              ', CAST(N'1996-01-01T00:00:00' AS SmallDateTime), N'略')
SET IDENTITY_INSERT [dbo].[TB_Grade] ON 

INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (1, N'04080101', N'040801', N'T080010401', N'C08002', 100, 60, 90, 85, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (2, N'04080102', N'040801', N'T080010401', N'C08002', 70, 70, 90, 84, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (3, N'04080103', N'040801', N'T080010401', N'C08002', 60, 60, 70, 67, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (4, N'04080104', N'040801', N'T080010401', N'C08002', 73, 81, 53, 61, 70, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (5, N'04080105', N'040801', N'T080010401', N'C08002', 45, 66, 80, 74, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (6, N'04080106', N'040801', N'T080010401', N'C08002', 78, 72, 79, 78, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (7, N'04080107', N'040801', N'T080010401', N'C08002', 33, 86, 90, 84, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (8, N'04080108', N'040801', N'T080010401', N'C08002', 90, 98, 87, 90, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (9, N'04080109', N'040801', N'T080010401', N'C08002', 88, 69, 96, 90, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (10, N'04080110', N'040801', N'T080010401', N'C08002', 87, 90, 97, 95, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (11, N'04080201', N'040802', N'T080010402', N'C08002', 70, 83, 92, 88, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (12, N'04080202', N'040802', N'T080010402', N'C08002', 75, 63, 74, 71.9, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (13, N'04080203', N'040802', N'T080010402', N'C08002', 66, 84, 70, 72.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (14, N'04080204', N'040802', N'T080010402', N'C08002', 63, 77, 62, 65.1, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (15, N'04080205', N'040802', N'T080010402', N'C08002', 45, 66, 56, 56.9, 90, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (16, N'04080206', N'040802', N'T080010402', N'C08002', 80, 72, 79, 77.7, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (17, N'04080207', N'040802', N'T080010402', N'C08002', 61, 66, 90, 82.3, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (18, N'04080208', N'040802', N'T080010402', N'C08002', 67, 78, 87, 83.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (19, N'04080209', N'040802', N'T080010402', N'C08002', 88, 69, 76, 75.8, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (20, N'04080210', N'040802', N'T080010402', N'C08002', 57, 72, 45, 51.6, 52, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (21, N'04080101', N'040801', N'T070020401', N'C07001', 75, 0, 92, 85.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (22, N'04080102', N'040801', N'T070020401', N'C07001', 65, 0, 74, 70.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (23, N'04080103', N'040801', N'T070020401', N'C07001', 68, 0, 70, 69.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (24, N'04080104', N'040801', N'T070020401', N'C07001', 88, 0, 62, 71.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (25, N'04080105', N'040801', N'T070020401', N'C07001', 79, 0, 56, 65.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (26, N'04080106', N'040801', N'T070020401', N'C07001', 80, 0, 79, 79.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (27, N'04080107', N'040801', N'T070020401', N'C07001', 42, 0, 38, 38.2, 45, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (28, N'04080108', N'040801', N'T070020401', N'C07001', 97, 0, 89, 92.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (29, N'04080109', N'040801', N'T070020401', N'C07001', 68, 0, 76, 73.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (30, N'04080110', N'040801', N'T070020401', N'C07001', 56, 0, 86, 74, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (31, N'04080101', N'040801', N'T080030401', N'C08004', 32, 0, 72, 48, 80, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (32, N'04080102', N'040801', N'T080030401', N'C08004', 60, 0, 74, 73.6, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (33, N'04080103', N'040801', N'T080030401', N'C08004', 72, 0, 70, 71.2, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (34, N'04080104', N'040801', N'T080030401', N'C08004', 88, 0, 86, 87.2, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (35, N'04080105', N'040801', N'T080030401', N'C08004', 76, 0, 50, 65.6, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (36, N'04080106', N'040801', N'T080030401', N'C08004', 80, 0, 79, 79.6, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (37, N'04080107', N'040801', N'T080030401', N'C08004', 90, 0, 38, 67.2, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (38, N'04080108', N'040801', N'T080030401', N'C08004', 87, 0, 84, 85.8, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (39, N'04080109', N'040801', N'T080030401', N'C08004', 78, 0, 76, 77.2, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (40, N'04080110', N'040801', N'T080030401', N'C08004', 56, 0, 80, 65.6, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (41, N'04080101', N'040801', N'T080040401', N'C08003', 85, 0, 75, 81, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (42, N'04020101', N'040201', N'T080040401', N'C08003', 90, 0, 84, 87.6, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (43, N'04020104', N'040201', N'T080040401', N'C08003', 82, 0, 90, 85.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (44, N'04080103', N'040801', N'T080040401', N'C08003', 78, 0, 76, 77.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (45, N'04080203', N'040802', N'T080040401', N'C08003', 77, 0, 58, 69.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (46, N'04080106', N'040801', N'T080040401', N'C08003', 86, 0, 89, 87.2, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (47, N'04080201', N'040802', N'T100050401', N'C10001', 80, 0, 90, 84, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (48, N'04080202', N'040802', N'T100050401', N'C10001', 75, 0, 74, 74.6, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (49, N'04020104', N'040201', N'T100050401', N'C10001', 69, 0, 90, 77.4, NULL, N'L')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (50, N'04080201', N'040802', N'T100020401', N'C10004', 65, 0, 95, 83, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (51, N'04080205', N'040802', N'T100020401', N'C10004', 77, 0, 84, 81.2, NULL, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (52, N'04080210', N'040802', N'T100020401', N'C10004', 77, 0, 84, 81.2, 0, N'U')
INSERT [dbo].[TB_Grade] ([GradeSeedID], [StuID], [ClassID], [CourseClassID], [CourseID], [CommonScore], [MiddleScore], [LastScore], [TotalScore], [RetestScore], [LockFlag]) VALUES (54, N'04080210', N'040802', N'T100020401', N'C08003', 80, 0, 80, 0, 0, N'L')
SET IDENTITY_INSERT [dbo].[TB_Grade] OFF
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04020101', N'T080040401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04020104', N'T080040401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04020104', N'T100050401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04020109', N'T100020402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080101', N'T070020399', CAST(N'2022-05-25T11:05:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080101', N'T080040401', CAST(N'2022-04-27T11:07:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080101', N'T100050401', CAST(N'2022-05-25T10:41:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080102', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080102', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080102', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080103', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080103', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080103', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080103', N'T080040401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080104', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080104', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080104', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080105', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080105', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080105', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080106', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080106', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080106', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080106', N'T080040401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080107', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080107', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080107', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080108', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080108', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080108', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080109', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080109', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080109', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080110', N'T070020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080110', N'T080010401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080110', N'T080030401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080201', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080201', N'T100020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080201', N'T100050401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080202', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080202', N'T100050401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080203', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080203', N'T080040401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080204', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080205', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080205', N'T100020401', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080206', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080207', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080208', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080209', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_SelectCourse] ([StuID], [CourseClassID], [SelectDate]) VALUES (N'04080210', N'T080010402', CAST(N'2022-03-16T10:48:00' AS SmallDateTime))
INSERT [dbo].[TB_Spec] ([SpecID], [SpecName], [DeptID], [SpecScript]) VALUES (N'0200', N'软件工程            ', N'02', N'略略')
INSERT [dbo].[TB_Spec] ([SpecID], [SpecName], [DeptID], [SpecScript]) VALUES (N'0201', N'机电一体化          ', N'02', N'略')
INSERT [dbo].[TB_Spec] ([SpecID], [SpecName], [DeptID], [SpecScript]) VALUES (N'0301', N'应用电子技术        ', N'03', N'略')
INSERT [dbo].[TB_Spec] ([SpecID], [SpecName], [DeptID], [SpecScript]) VALUES (N'0801', N'计算机网络技术      ', N'08', N'略')
INSERT [dbo].[TB_Spec] ([SpecID], [SpecName], [DeptID], [SpecScript]) VALUES (N'0802', N'动漫设计            ', N'08', N'略')
INSERT [dbo].[TB_Spec] ([SpecID], [SpecName], [DeptID], [SpecScript]) VALUES (N'0803', N'软件技术            ', N'08', N'略')
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020099', N'A亮亮   ', N'2004', N'2007', N'02', N'040201', N'M', CAST(N'1984-12-12T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020101', N'周灵灵  ', N'2004', N'2007', N'02', N'040201', N'F', CAST(N'1984-12-12T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 87)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020102', N'余红燕  ', N'2004', N'2007', N'02', N'040201', N'F', CAST(N'1985-12-06T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020103', N'左秋霞  ', N'2004', N'2007', N'02', N'040201', N'F', CAST(N'1985-05-01T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020104', N'汪德荣  ', N'2004', N'2007', N'02', N'040201', N'M', CAST(N'1984-12-13T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 162)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020105', N'刘成波  ', N'2004', N'2007', N'02', N'040201', N'M', CAST(N'1984-07-04T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020106', N'郭昌盛  ', N'2004', N'2007', N'02', N'040201', N'M', CAST(N'1984-04-16T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020107', N'陈玲玲  ', N'2004', N'2007', N'02', N'040201', N'F', CAST(N'1984-10-18T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020108', N'周炳才  ', N'2004', N'2007', N'02', N'040201', N'M', CAST(N'1986-01-22T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020109', N'李克寅  ', N'2004', N'2007', N'02', N'040201', N'F', CAST(N'1985-02-19T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04020110', N'刘争艳  ', N'2004', N'2007', N'02', N'040201', N'M', CAST(N'1984-08-18T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080101', N'任正非  ', N'2004', N'2007', N'08', N'040801', N'M', CAST(N'1984-01-10T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 297)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080102', N'王倩    ', N'2004', N'2007', N'08', N'040801', N'F', CAST(N'1985-05-16T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 228)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080103', N'戴丽    ', N'2004', N'2007', N'08', N'040801', N'F', CAST(N'1984-12-01T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 284)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080104', N'孙军团  ', N'2004', N'2007', N'08', N'040801', N'M', CAST(N'1983-02-03T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 208)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080105', N'郑志    ', N'2004', N'2007', N'08', N'040801', N'M', CAST(N'1986-11-04T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 200)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080106', N'龚玲玲  ', N'2004', N'2007', N'08', N'040801', N'F', CAST(N'1984-11-10T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 323)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080107', N'李铁    ', N'2004', N'2007', N'08', N'040801', N'M', CAST(N'1985-01-08T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 188)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080108', N'戴安娜  ', N'2004', N'2007', N'08', N'040801', N'F', CAST(N'1984-12-22T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 267)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080109', N'陈淋淋  ', N'2004', N'2007', N'08', N'040801', N'F', CAST(N'1985-03-19T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 240)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080110', N'司马光  ', N'2004', N'2007', N'08', N'040801', N'M', CAST(N'1984-08-03T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 234)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080201', N'张金玲  ', N'2004', N'2007', N'08', N'040802', N'F', CAST(N'1984-03-26T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 255)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080202', N'王婷婷  ', N'2004', N'2007', N'08', N'040802', N'F', CAST(N'1985-04-12T00:00:00' AS SmallDateTime), N'123456', N'略', N'211900', 146)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080203', N'石江安  ', N'2004', N'2007', N'08', N'040802', N'M', CAST(N'1984-11-01T00:00:00' AS SmallDateTime), N'123456', N'江苏省宝应市虹彩小区2幢101', N'225800', 141)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080204', N'陈建伟  ', N'2004', N'2007', N'08', N'040802', N'M', CAST(N'1984-12-13T00:00:00' AS SmallDateTime), N'123456', N'江苏省大丰市黄林乡五村56号', N'224200', 65)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080205', N'袁中标  ', N'2004', N'2007', N'08', N'040802', N'M', CAST(N'1985-10-04T00:00:00' AS SmallDateTime), N'123456', N'安徽省芜湖市笆斗街60号', N'241000', 138)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080206', N'崔莎莎  ', N'2004', N'2007', N'08', N'040802', N'F', CAST(N'1984-04-10T00:00:00' AS SmallDateTime), N'123456', N'略', N'244100', 77)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080207', N'丁承华  ', N'2004', N'2007', N'08', N'040802', N'M', CAST(N'1985-11-18T00:00:00' AS SmallDateTime), N'123456', N'略', N'311200', 82)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080208', N'刘颖    ', N'2004', N'2007', N'08', N'040802', N'F', CAST(N'1986-02-22T00:00:00' AS SmallDateTime), N'123456', N'江苏省江阴市虹桥一村7幢201', N'214400', 83)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080209', N'刘玉芹  ', N'2004', N'2007', N'08', N'040802', N'F', CAST(N'1985-08-09T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', 75)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'04080210', N'韦涛    ', N'2004', N'2007', N'08', N'040802', N'M', CAST(N'1985-03-06T00:00:00' AS SmallDateTime), N'123456', N'安徽省界首市河北乡新黄村31号', N'236500', 132)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030201', N'刘惠凤  ', N'2005', N'2008', N'03', N'050302', N'F', CAST(N'1986-08-12T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030202', N'孙嘉珍  ', N'2005', N'2008', N'03', N'050302', N'F', CAST(N'1987-12-06T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030203', N'单瑞中  ', N'2005', N'2008', N'03', N'050302', N'M', CAST(N'1985-12-11T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030204', N'陈颂    ', N'2005', N'2008', N'03', N'050302', N'M', CAST(N'1986-12-03T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030205', N'包学诚  ', N'2005', N'2008', N'03', N'050302', N'M', CAST(N'1985-07-04T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030206', N'胡春霞  ', N'2005', N'2008', N'03', N'050302', N'F', CAST(N'1985-05-19T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030207', N'贾彩丽  ', N'2005', N'2008', N'03', N'050302', N'F', CAST(N'1985-09-08T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030208', N'高文欢  ', N'2005', N'2008', N'03', N'050302', N'M', CAST(N'1987-12-02T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030209', N'沈伟    ', N'2005', N'2008', N'03', N'050302', N'F', CAST(N'1986-02-09T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05030210', N'马霞    ', N'2005', N'2008', N'03', N'050302', N'M', CAST(N'1986-08-18T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080101', N'李娟娟  ', N'2005', N'2008', N'08', N'050801', N'F', CAST(N'1985-07-08T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080102', N'刘晓燕  ', N'2005', N'2008', N'08', N'050801', N'F', CAST(N'1986-06-06T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080103', N'高原    ', N'2005', N'2008', N'08', N'050801', N'M', CAST(N'1985-01-11T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080104', N'毛振兴  ', N'2005', N'2008', N'08', N'050801', N'M', CAST(N'1985-02-13T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080105', N'陈立    ', N'2005', N'2008', N'08', N'050801', N'M', CAST(N'1985-06-24T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080106', N'朱凤    ', N'2005', N'2008', N'08', N'050801', N'F', CAST(N'1985-04-10T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080107', N'王小丽  ', N'2005', N'2008', N'08', N'050801', N'F', CAST(N'1985-10-08T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080108', N'曹勇    ', N'2005', N'2008', N'08', N'050801', N'M', CAST(N'1986-12-22T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080109', N'赵启悦  ', N'2005', N'2008', N'08', N'050801', N'F', CAST(N'1985-12-09T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080110', N'石慧    ', N'2005', N'2008', N'08', N'050801', N'M', CAST(N'1986-08-08T00:00:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Student] ([StuID], [StuName], [EnrollYear], [GradYear], [DeptID], [ClassID], [Sex], [Birthday], [SPassword], [StuAddress], [ZipCode], [TotalGrade]) VALUES (N'05080111', N'陈亮亮  ', N'2005', N'2008', N'08', N'050801', N'M', CAST(N'2022-04-27T11:31:00' AS SmallDateTime), N'123456', N'略', N'214400', NULL)
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T02000', N'C亮亮   ', N'02', N'M', CAST(N'2001-11-19T00:00:00' AS SmallDateTime), N'123456', N'T2')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T02001', N'程靖    ', N'02', N'F', CAST(N'1974-08-27T00:00:00' AS SmallDateTime), N'123456', N'T2')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T07002', N'沈丽    ', N'07', N'M', CAST(N'1976-06-06T00:00:00' AS SmallDateTime), N'123456', N'T1')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T08001', N'陈玲    ', N'08', N'F', CAST(N'1968-12-02T00:00:00' AS SmallDateTime), N'123456', N'T3')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T08002', N'李琳分  ', N'08', N'M', CAST(N'1972-10-10T00:00:00' AS SmallDateTime), N'123456', N'T3')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T08003', N'龙永图  ', N'08', N'M', CAST(N'1976-06-23T00:00:00' AS SmallDateTime), N'123456', N'T2')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T08004', N'黄三清  ', N'08', N'F', CAST(N'1983-07-13T00:00:00' AS SmallDateTime), N'123456', N'T1')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T08005', N'韩汉    ', N'08', N'M', CAST(N'1956-03-18T00:00:00' AS SmallDateTime), N'123456', N'T4')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T10002', N'沈天一  ', N'10', N'F', CAST(N'1970-11-16T00:00:00' AS SmallDateTime), N'123456', N'T3')
INSERT [dbo].[TB_Teacher] ([TeacherID], [TeacherName], [DeptID], [Sex], [Birthday], [TPassword], [TitleID]) VALUES (N'T10005', N'曾远    ', N'10', N'M', CAST(N'1986-06-03T00:00:00' AS SmallDateTime), N'123456', N'T1')
INSERT [dbo].[TB_TeachingYear] ([TeachingYearID], [TeachingYearName]) VALUES (N'2004', N'2004-2005学年')
INSERT [dbo].[TB_TeachingYear] ([TeachingYearID], [TeachingYearName]) VALUES (N'2005', N'2005-2006学年')
INSERT [dbo].[TB_TeachingYear] ([TeachingYearID], [TeachingYearName]) VALUES (N'2006', N'2006-2007学年')
INSERT [dbo].[TB_TeachingYear] ([TeachingYearID], [TeachingYearName]) VALUES (N'2007', N'2007-2008学年')
INSERT [dbo].[TB_TeachingYear] ([TeachingYearID], [TeachingYearName]) VALUES (N'2008', N'2008-2009学年')
INSERT [dbo].[TB_Term] ([TermID], [TermName]) VALUES (N'T1', N'第一学期')
INSERT [dbo].[TB_Term] ([TermID], [TermName]) VALUES (N'T2', N'第二学期')
INSERT [dbo].[TB_Term] ([TermID], [TermName]) VALUES (N'T3', N'第三学期')
INSERT [dbo].[TB_Term] ([TermID], [TermName]) VALUES (N'T4', N'第四学期')
INSERT [dbo].[TB_Term] ([TermID], [TermName]) VALUES (N'T5', N'第五学期')
INSERT [dbo].[TB_Term] ([TermID], [TermName]) VALUES (N'T6', N'第六学期')
INSERT [dbo].[TB_Title] ([TitleID], [TitleName]) VALUES (N'T1', N'助教    ')
INSERT [dbo].[TB_Title] ([TitleID], [TitleName]) VALUES (N'T2', N'讲师    ')
INSERT [dbo].[TB_Title] ([TitleID], [TitleName]) VALUES (N'T3', N'副教授  ')
INSERT [dbo].[TB_Title] ([TitleID], [TitleName]) VALUES (N'T4', N'教授    ')
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__TB_Cours__9526E2772CE5DCDE]    Script Date: 2022/5/25 12:07:08 ******/
ALTER TABLE [dbo].[TB_Course] ADD UNIQUE NONCLUSTERED 
(
	[CourseName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TB_Course] ADD  DEFAULT ((0)) FOR [CourseGrade]
GO
ALTER TABLE [dbo].[TB_Course] ADD  DEFAULT ((0)) FOR [LessonTime]
GO
ALTER TABLE [dbo].[TB_CourseClass] ADD  DEFAULT ((10)) FOR [CommonPart]
GO
ALTER TABLE [dbo].[TB_CourseClass] ADD  DEFAULT ((20)) FOR [MiddlePart]
GO
ALTER TABLE [dbo].[TB_CourseClass] ADD  DEFAULT ((70)) FOR [LastPart]
GO
ALTER TABLE [dbo].[TB_CourseClass] ADD  DEFAULT ((60)) FOR [MaxNumber]
GO
ALTER TABLE [dbo].[TB_CourseClass] ADD  DEFAULT ((0)) FOR [SelectedNumber]
GO
ALTER TABLE [dbo].[TB_CourseClass] ADD  DEFAULT ('U') FOR [FullFlag]
GO
ALTER TABLE [dbo].[TB_Grade] ADD  DEFAULT ((0)) FOR [CommonScore]
GO
ALTER TABLE [dbo].[TB_Grade] ADD  DEFAULT ((0)) FOR [MiddleScore]
GO
ALTER TABLE [dbo].[TB_Grade] ADD  DEFAULT ((0)) FOR [LastScore]
GO
ALTER TABLE [dbo].[TB_Grade] ADD  DEFAULT ((0)) FOR [TotalScore]
GO
ALTER TABLE [dbo].[TB_Grade] ADD  DEFAULT ((0)) FOR [RetestScore]
GO
ALTER TABLE [dbo].[TB_Grade] ADD  DEFAULT ('U') FOR [LockFlag]
GO
ALTER TABLE [dbo].[TB_SelectCourse] ADD  DEFAULT (getdate()) FOR [SelectDate]
GO
ALTER TABLE [dbo].[TB_Student] ADD  DEFAULT ('M') FOR [Sex]
GO
ALTER TABLE [dbo].[TB_Student] ADD  DEFAULT ('123456') FOR [SPassword]
GO
ALTER TABLE [dbo].[TB_Student] ADD  DEFAULT (NULL) FOR [TotalGrade]
GO
ALTER TABLE [dbo].[TB_Teacher] ADD  DEFAULT ('M') FOR [Sex]
GO
ALTER TABLE [dbo].[TB_Teacher] ADD  DEFAULT ('123456') FOR [TPassword]
GO
ALTER TABLE [dbo].[TB_Class]  WITH CHECK ADD FOREIGN KEY([DeptID])
REFERENCES [dbo].[TB_Dept] ([DeptID])
GO
ALTER TABLE [dbo].[TB_Class]  WITH CHECK ADD FOREIGN KEY([TeacherID])
REFERENCES [dbo].[TB_Teacher] ([TeacherID])
GO
ALTER TABLE [dbo].[TB_Course]  WITH CHECK ADD FOREIGN KEY([DeptID])
REFERENCES [dbo].[TB_Dept] ([DeptID])
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD FOREIGN KEY([CourseID])
REFERENCES [dbo].[TB_Course] ([CourseID])
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD FOREIGN KEY([TeacherID])
REFERENCES [dbo].[TB_Teacher] ([TeacherID])
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD FOREIGN KEY([TeachingYearID])
REFERENCES [dbo].[TB_TeachingYear] ([TeachingYearID])
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD FOREIGN KEY([TermID])
REFERENCES [dbo].[TB_Term] ([TermID])
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD FOREIGN KEY([ClassID])
REFERENCES [dbo].[TB_Class] ([ClassID])
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD FOREIGN KEY([CourseClassID])
REFERENCES [dbo].[TB_CourseClass] ([CourseClassID])
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD FOREIGN KEY([CourseID])
REFERENCES [dbo].[TB_Course] ([CourseID])
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD FOREIGN KEY([StuID])
REFERENCES [dbo].[TB_Student] ([StuID])
GO
ALTER TABLE [dbo].[TB_SelectCourse]  WITH CHECK ADD FOREIGN KEY([CourseClassID])
REFERENCES [dbo].[TB_CourseClass] ([CourseClassID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TB_SelectCourse]  WITH CHECK ADD FOREIGN KEY([StuID])
REFERENCES [dbo].[TB_Student] ([StuID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TB_Spec]  WITH CHECK ADD FOREIGN KEY([DeptID])
REFERENCES [dbo].[TB_Dept] ([DeptID])
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD FOREIGN KEY([ClassID])
REFERENCES [dbo].[TB_Class] ([ClassID])
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD FOREIGN KEY([DeptID])
REFERENCES [dbo].[TB_Dept] ([DeptID])
GO
ALTER TABLE [dbo].[TB_Teacher]  WITH CHECK ADD FOREIGN KEY([DeptID])
REFERENCES [dbo].[TB_Dept] ([DeptID])
GO
ALTER TABLE [dbo].[TB_Teacher]  WITH CHECK ADD FOREIGN KEY([TitleID])
REFERENCES [dbo].[TB_Title] ([TitleID])
GO
ALTER TABLE [dbo].[TB_Course]  WITH CHECK ADD CHECK  (([CourseGrade]>=(0)))
GO
ALTER TABLE [dbo].[TB_Course]  WITH CHECK ADD CHECK  (([CourseID] like 'C[0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_Course]  WITH CHECK ADD CHECK  (([LessonTime]>=(0)))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD CHECK  (([CommonPart]>=(0)))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD CHECK  (([CourseClassID] like 'T[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD CHECK  (([FullFlag]='U' OR [FullFlag]='F'))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD CHECK  (([LastPart]>=(0)))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD CHECK  (([MaxNumber]>=(0)))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD CHECK  (([MiddlePart]>=(0)))
GO
ALTER TABLE [dbo].[TB_CourseClass]  WITH CHECK ADD  CONSTRAINT [CK_SumOfParts] CHECK  (((([CommonPart]+[MiddlePart])+[LastPart])=(100)))
GO
ALTER TABLE [dbo].[TB_CourseClass] CHECK CONSTRAINT [CK_SumOfParts]
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD CHECK  (([CommonScore]>=(0) AND [CommonScore]<=(100)))
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD CHECK  (([LastScore]>=(0) AND [LastScore]<=(100)))
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD CHECK  (([LockFlag]='L' OR [LockFlag]='U'))
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD CHECK  (([MiddleScore]>=(0) AND [MiddleScore]<=(100)))
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD CHECK  (([RetestScore]>=(0) AND [RetestScore]<=(100)))
GO
ALTER TABLE [dbo].[TB_Grade]  WITH CHECK ADD CHECK  (([TotalScore]>=(0) AND [TotalScore]<=(100)))
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD CHECK  (([EnrollYear] like '[0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD CHECK  (([GradYear] like '[0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD CHECK  (([StuID] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD CHECK  (([ZipCode] like '[0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_Student]  WITH CHECK ADD CHECK  (([Sex]='F' OR [Sex]='M'))
GO
ALTER TABLE [dbo].[TB_Teacher]  WITH CHECK ADD CHECK  (([TeacherID] like 'T[0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[TB_Teacher]  WITH CHECK ADD CHECK  (([Sex]='F' OR [Sex]='M'))
GO
/****** Object:  StoredProcedure [dbo].[SP_CourseClassGradeQuery]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_CourseClassGradeQuery] @CourseClassID CHAR ( 10 ) AS SELECT
	GradeSeedID,
	TG.StuID,
	StuName,
	CommonScore,
	MiddleScore,   LastScore,
	TotalScore,
	LockFlag     
FROM
	TB_Grade TG,
	TB_Student TS     
WHERE
	TG.StuID= TS.StuID 
	AND CourseClassID =@CourseClassID
GO
/****** Object:  StoredProcedure [dbo].[SP_GradeProc]    Script Date: 2022/5/25 12:07:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GradeProc] @CourseClassID CHAR(10)  
AS 
------定义课程考试比例系数变量并获取相应的值------- 
DECLARE @CPart REAL,@MPart REAL,@LPart REAL 
SELECT @CPart=CommonPart,@MPart=MiddlePart,@LPart=LastPart 
FROM TB_CourseClass 
WHERE CourseClassID=@CourseClassID 
------定义用来存放平时、期中、期末、总评成绩变量------- 
DECLARE @CScore REAL,@MScore REAL,@LScore REAL,@TotalScore REAL 
------------------声明游标------------------ 
DECLARE CUR_GradeProc CURSOR FOR 
SELECT CommonScore,MiddleScore,LastScore FROM TB_Grade 
WHERE CourseClassID=@CourseClassID ORDER BY StuID
------------------打开游标------------------ 
OPEN CUR_GradeProc 
-----------循环提取游标中成绩并处理------------ 
FETCH NEXT FROM CUR_GradeProc INTO @CScore,@MScore,@LScore 
WHILE @@FETCH_STATUS = 0 
BEGIN 
SET @TotalScore= ROUND((@CScore*@CPart+@MScore*@MPart+@LScore*@LPart)/100,0) 
UPDATE TB_Grade SET TotalScore=@TotalScore 
WHERE CURRENT OF CUR_GradeProc 
FETCH NEXT FROM CUR_GradeProc INTO @CScore,@MScore,@LScore 
END 
----------------关闭释放游标---------------- 
CLOSE CUR_GradeProc 
DEALLOCATE CUR_GradeProc
GO
