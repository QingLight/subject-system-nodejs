var express = require('express');
var app = express();

var sql = require("mssql");
// config for your database
var config = {
    user: 'sa', // update me
    password: '8650', // update me
    server: 'localhost', // update me
    database: 'DB_TeachingMS', // update me
    encrypt: false //  不加上这个会报错：connect error
};

var server = app.listen(8081, function () {
    console.log('Server is running http:127.0.0.1:8081');
});

// return_type： 0 -> 查询语句返回数据集， 1 -> 插入语句返回success
function execute(req, res, sql_statement, return_type = 0) {
    sql.connect(config, function (err) {
        // if (err) console.log(err);
        if (err) {
            console.log('数据库连接失败');
            res.send(err)
            return;
        }

        // create Request object
        var request = new sql.Request();

        // query to the database and get the records
        request.query(sql_statement, function (err, recordset) {
            // if (err) console.log(err)
            if (err) {
                console.log('查询失败');
                res.send(err)
                return;
            }
            // send records as a response
            if (return_type == 1) {
                res.send('success')
                return
            }
            res.send(recordset.recordsets[0]);
        });
    });
}

app.get('/query/student/login', function (req, res) {
    const sql_statement = `select * from TB_Student where StuID = ${req.query.StuID}`
    execute(req, res, sql_statement)
});

app.get('/query/teacher/login', function (req, res) {
    const sql_statement = `select * from TB_Teacher where TeacherID = '${req.query.TeacherID}'`
    execute(req, res, sql_statement)
});

app.get('/query/teacher/score', function (req, res) {
    const sql_statement = `
    SELECT
        a.CourseName,
        b.CourseClassID,
        round( AVG ( b.TotalScore ), 2 ) AverageScore 
    FROM
        TB_Course a,
        TB_Grade b,
        TB_CourseClass c 
    WHERE
        TeacherID = '${req.query.TeacherID}' 
        AND b.CourseClassID = c.CourseClassID 
        AND c.CourseID = a.CourseID 
    GROUP BY
        b.CourseClassID,
        a.CourseName
    `
    execute(req, res, sql_statement)
});

app.get('/query/student/score', function (req, res) {
    const sql_statement = `
    SELECT
        TG.CourseID,
        CourseName,
        TotalScore,
        RetestScore 
    FROM
        TB_Grade TG
        JOIN TB_Course TC ON TG.CourseID= TC.CourseID 
        AND StuID = '${req.query.StuID}'
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/CCName', function (req, res) {
    const sql_statement = `
    SELECT
        CourseClassID,
        CourseName + '|' + TeachingTime + '|' + TeachingPlace AS CCName
    FROM
        TB_CourseClass TCC,
        TB_Course TC
    WHERE
        TCC.CourseID= TC.CourseID 
        AND TeacherID = '${req.query.TeacherID}'
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/CCGrade', function (req, res) {
    const sql_statement = `
    SELECT
        TG.StuID,
        StuName,
        CommonScore,
        MiddleScore,
        LastScore,
        TotalScore
    FROM
        TB_Grade TG,
        TB_Student TS
    WHERE
        TG.StuID= TS.StuID 
        AND CourseClassID = '${req.query.CourseClassID}'
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/teacherInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TeacherID, TeacherName
    FROM
        TB_Teacher
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/classInfo', function (req, res) {
    const sql_statement = `
    SELECT
        ClassID, ClassName
    FROM
        TB_Class
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/studentInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TS.StuID, TS.StuName
    FROM
        TB_Class TC,
        TB_Student TS 
    WHERE
        TC.ClassID= TS.ClassID 
        AND TS.ClassID= '${req.query.ClassID}'
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/studentGrade', function (req, res) {
    const sql_statement = `
    SELECT
        TG.StuID,
        TS.StuName,
        CommonScore,
        MiddleScore,
        LastScore,
        TotalScore 
    FROM
        TB_Grade TG, TB_Student TS
    WHERE
        TG.StuID = '${req.query.StuID}' AND TS.StuID = TG.StuID
    `
    execute(req, res, sql_statement)
});

app.get('/query/student/availableCourse', function (req, res) {
    const sql_statement = `
    SELECT
        CourseClassID,
        CourseName,
        TeacherName,
        TeachingPlace,
        TeachingTime,
        MaxNumber,
        SelectedNumber 
    FROM
        TB_CourseClass TCC,
        TB_Course TC,
        TB_Teacher TT 
    WHERE
        TCC.CourseID= TC.CourseID 
        AND TCC.TeacherID= TT.TeacherID 
        AND FullFlag = 'U' 
        AND CourseClassID NOT IN ( SELECT CourseClassID FROM TB_SelectCourse WHERE StuID = '${req.query.StuID}' )
    `
    execute(req, res, sql_statement)
});

app.get('/add/student/selectCourse', function (req, res) {
    const sql_statement = `
    INSERT INTO TB_SelectCourse ( StuID, CourseClassID )
    VALUES
        ( '${req.query.StuID}', '${req.query.ClassID}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/student/selectedInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TSC.CourseClassID,
        CourseName,
        TeacherName,
        TeachingPlace,
        TeachingTime,
        MaxNumber,
        SelectedNumber 
    FROM
        TB_SelectCourse TSC,
        TB_CourseClass TCC,
        TB_Course TC,
        TB_Teacher TT 
    WHERE
        TSC.CourseClassID= TCC.CourseClassID 
        AND TCC.CourseID= TC.CourseID 
        AND TCC.TeacherID= TT.TeacherID 
        AND StuID = '${req.query.StuID}'
    `
    execute(req, res, sql_statement)
});

app.get('/delete/student/dropCourse', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_SelectCourse 
    WHERE
        StuID = '${req.query.StuID}' 
        AND CourseClassID = '${req.query.ClassID}'
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/courseClassGrade', function (req, res) {
    const sql_statement = `exec SP_CourseClassGradeQuery '${req.query.ClassID}' `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateScore', function (req, res) {
    const sql_statement = `
    UPDATE 
        TB_Grade
    SET
        CommonScore = '${req.query.CommonScore}', 
        MiddleScore = '${req.query.MiddleScore}',
        LastScore = '${req.query.LastScore}',
        TotalScore = '${req.query.TotalScore}',
        LockFlag = '${req.query.LockFlag}'
    WHERE GradeSeedID = '${req.query.GradeSeedID}'
    `
    // console.log(sql_statement)
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/gradeProc', function (req, res) {
    const sql_statement = `exec SP_GradeProc '${req.query.CourseClassID}' `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/getDeptName', function (req, res) {
    const sql_statement = `
    SELECT
        DeptID,
        DeptName 
    FROM
        TB_Dept
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/getTeacherName', function (req, res) {
    const sql_statement = `
    SELECT
        TeacherID,
        TeacherName 
    FROM
        TB_Teacher
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/classAdd', function (req, res) {
    const sql_statement = `
    INSERT INTO TB_Class (ClassID,ClassName,DeptID,TeacherID)
    VALUES
    ( '${req.query.ClassID}', '${req.query.ClassName}', '${req.query.DeptID}', '${req.query.TeacherID}' ) `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/getClassInfo', function (req, res) {
    const sql_statement = `
    SELECT
        ClassID,
        ClassName,
        DeptName,
        TeacherName,
        TC.DeptID,
        TC.TeacherID
    FROM
        TB_Class TC,
        TB_Dept TD,
        TB_Teacher TT 
    WHERE
        TC.DeptID= TD.DeptID 
        AND TC.TeacherID= TT.TeacherID
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateClass', function (req, res) {
    const sql_statement = `
    UPDATE 
        TB_Class
    SET
        ClassName = '${req.query.ClassName}', 
        DeptID = '${req.query.DeptID}',
        TeacherID = '${req.query.TeacherID}'
    WHERE ClassID = '${req.query.ClassID}' 
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/deleteClass', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_Class 
    WHERE
        ClassID = ${req.query.ClassID}
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/stuAdd', function (req, res) {
    const { StuID, StuName, EnrollYear, GradYear, DeptID, ClassID, Sex, Birthday, SPassword, StuAddress, ZipCode } = req.query
    const sql_statement = `
    INSERT INTO TB_Student (StuID, StuName, EnrollYear, GradYear, DeptID, ClassID, Sex, Birthday, SPassword, StuAddress, ZipCode)
    VALUES
    ( '${StuID}', '${StuName}', '${EnrollYear}', '${GradYear}', '${DeptID}', '${ClassID}', '${Sex}', '${Birthday}', '${SPassword}', '${StuAddress}', '${ZipCode}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getStuInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TS.*,
        TC.ClassName,
        TD.DeptName
    FROM
        TB_Class TC,
        TB_Dept TD,
        TB_Student TS 
    WHERE
        TC.ClassID= TS.ClassID 
        AND TD.DeptID= TS.DeptID
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateStu', function (req, res) {
    const { StuID, StuName, EnrollYear, GradYear, DeptID, ClassID, Sex, Birthday, SPassword, StuAddress, ZipCode } = req.query
    const sql_statement = `
    UPDATE 
        TB_Student
    SET
        StuName = '${StuName}', 
        EnrollYear = '${EnrollYear}',
        GradYear = '${GradYear}',
        DeptID = '${DeptID}',
        ClassID = '${ClassID}',
        Sex = '${Sex}',
        Birthday = '${Birthday}',
        SPassword = '${SPassword}',
        StuAddress = '${StuAddress}',
        ZipCode = '${ZipCode}'
    WHERE StuID = '${StuID}' 
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deleteStu', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_Student 
    WHERE
        StuID = ${req.query.StuID}
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deptAdd', function (req, res) {
    const { DeptID, DeptName, DeptSetDate, DeptScript } = req.query
    const sql_statement = `
    INSERT INTO TB_Dept ( DeptID, DeptName, DeptSetDate, DeptScript)
    VALUES
    ( '${DeptID}', '${DeptName}', '${DeptSetDate}', '${DeptScript}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getDeptInfo', function (req, res) {
    const sql_statement = `
    SELECT
        *
    FROM
        TB_Dept
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateDept', function (req, res) {
    const sql_statement = `
    UPDATE 
        TB_Dept
    SET
        DeptID = '${req.query.DeptID}',
        DeptName = '${req.query.DeptName}', 
        DeptSetDate = '${req.query.DeptSetDate}',
        DeptScript = '${req.query.DeptScript}'
    WHERE DeptID = '${req.query.DeptID}' 
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deleteDept', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_Dept 
    WHERE
    DeptID = ${req.query.DeptID}
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/specAdd', function (req, res) {
    const { SpecID, SpecName, DeptID, SpecScript } = req.query
    const sql_statement = `
    INSERT INTO TB_Spec ( SpecID, SpecName, DeptID, SpecScript )
    VALUES
    ( '${SpecID}', '${SpecName}', '${DeptID}', '${SpecScript}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getSpecInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TB_Spec.*,
        TB_Dept.DeptName
    FROM
        TB_Spec,
        TB_Dept
    WHERE
        TB_Spec.DeptID = TB_Dept.DeptID
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateSpec', function (req, res) {
    const sql_statement = `
    UPDATE 
        TB_Spec
    SET
        SpecID = '${req.query.SpecID}',
        SpecName = '${req.query.SpecName}', 
        DeptID = '${req.query.DeptID}',
        SpecScript = '${req.query.SpecScript}'
    WHERE SpecID = '${req.query.SpecID}' 
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deleteSpec', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_Spec 
    WHERE
        SpecID = ${req.query.SpecID}
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getTitleInfo', function (req, res) {
    const sql_statement = `
    SELECT
        *
    FROM
        TB_Title
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/teacherAdd', function (req, res) {
    const { TeacherID, TeacherName, DeptID, Sex, Birthday, TPassword, TitleID } = req.query
    const sql_statement = `
    INSERT INTO 
        TB_Teacher ( TeacherID, TeacherName, DeptID, Sex, Birthday, TPassword, TitleID )
    VALUES
        ( '${TeacherID}', '${TeacherName}', '${DeptID}', '${Sex}','${Birthday}','${TPassword}','${TitleID}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getTeacherInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TTR.*,
        TDT.DeptName,
        TTE.TitleName 
    FROM
        TB_Teacher TTR,
        TB_Dept TDT,
        TB_Title TTE 
    WHERE
        TDT.DeptID = TTR.DeptID 
        AND TTE.TitleID = TTR.TitleID
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateTeacher', function (req, res) {
    const { TeacherID, TeacherName, DeptID, Sex, Birthday, TPassword, TitleID } = req.query
    const sql_statement = `
    UPDATE 
        TB_Teacher
    SET
        TeacherName = '${TeacherName}', 
        DeptID = '${DeptID}',
        Sex = '${Sex}',
        Birthday = '${Birthday}',
        TPassword = '${TPassword}',
        TitleID = '${TitleID}'
    WHERE TeacherID = '${TeacherID}'
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deleteTeacher', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_Teacher 
    WHERE
        TeacherID = '${req.query.TeacherID}'
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/courseAdd', function (req, res) {
    const { CourseID, CourseName, DeptID, CourseGrade, LessonTime, CourseOutline } = req.query
    const sql_statement = `
    INSERT INTO 
        TB_Course ( CourseID, CourseName, DeptID, CourseGrade, LessonTime, CourseOutline )
    VALUES
        ( '${CourseID}', '${CourseName}', '${DeptID}', '${CourseGrade}','${LessonTime}','${CourseOutline}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getCourseInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TC.*,
        TD.DeptName
    FROM
        TB_Course TC,
        TB_Dept TD
    WHERE
        TD.DeptID = TC.DeptID 
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateCourse', function (req, res) {
    const { CourseID, CourseName, DeptID, CourseGrade, LessonTime, CourseOutline } = req.query
    const sql_statement = `
    UPDATE 
        TB_Course
    SET
        CourseName = '${CourseName}',
        DeptID = '${DeptID}',
        CourseGrade = '${CourseGrade}',
        LessonTime = '${LessonTime}',
        CourseOutline = '${CourseOutline}'
    WHERE CourseID = '${CourseID}'
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deleteCourse', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_Course 
    WHERE
        CourseID = '${req.query.CourseID}'
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getTeacherYearInfo', function (req, res) {
    const sql_statement = `SELECT * FROM TB_TeachingYear`
    execute(req, res, sql_statement)
});

app.get('/query/teacher/getTermInfo', function (req, res) {
    const sql_statement = `SELECT * FROM TB_Term`
    execute(req, res, sql_statement)
});

app.get('/query/teacher/courseClassAdd', function (req, res) {
    const { CourseClassID, CourseID, TeacherID, TeachingYearID, TermID, TeachingPlace, TeachingTime, CommonPart, MiddlePart, LastPart, MaxNumber, SelectedNumber, FullFlag } = req.query
    const sql_statement = `
    INSERT INTO 
        TB_CourseClass ( CourseClassID, CourseID, TeacherID, TeachingYearID, TermID, TeachingPlace, TeachingTime, CommonPart, MiddlePart, LastPart, MaxNumber, SelectedNumber, FullFlag )
    VALUES
        ( '${CourseClassID}', '${CourseID}', '${TeacherID}', '${TeachingYearID}', '${TermID}', '${TeachingPlace}', '${TeachingTime}', '${CommonPart}', '${MiddlePart}', '${LastPart}', '${MaxNumber}', '${SelectedNumber}', '${FullFlag}' )
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/getCourseClassInfo', function (req, res) {
    const sql_statement = `
    SELECT
        TCC.*,
        TC.CourseName,
        TTR.TeacherName,
        TT.TermName,
        TTY.TeachingYearName
    FROM
        TB_Course TC,
        TB_CourseClass TCC,
        TB_Term TT,
        TB_Teacher TTR,
        TB_TeachingYear TTY 
    WHERE
        TCC.CourseID = TC.CourseID 
        AND TCC.TermID = TT.TermID 
        AND TCC.TeacherID = TTR.TeacherID 
        AND TTY.TeachingYearID= TCC.TeachingYearID
    `
    execute(req, res, sql_statement)
});

app.get('/query/teacher/updateCourseClass', function (req, res) {
    const { CourseClassID, CourseID, TeacherID, TeachingYearID, TermID, TeachingPlace, TeachingTime, CommonPart, MiddlePart, LastPart, MaxNumber, SelectedNumber, FullFlag } = req.query
    const sql_statement = `
    UPDATE 
        TB_CourseClass
    SET
        CourseID = '${CourseID}',
        TeacherID = '${TeacherID}',
        TeachingYearID = '${TeachingYearID}',
        TermID = '${TermID}',
        TeachingPlace = '${TeachingPlace}',
        TeachingTime = '${TeachingTime}',
        CommonPart = '${CommonPart}',
        MiddlePart = '${MiddlePart}',
        LastPart = '${LastPart}',
        MaxNumber = '${MaxNumber}',
        SelectedNumber = '${SelectedNumber}',
        FullFlag = '${FullFlag}'
    WHERE CourseClassID = '${CourseClassID}'
    `
    execute(req, res, sql_statement, 1)
});

app.get('/query/teacher/deleteCourseClass', function (req, res) {
    const sql_statement = `
    DELETE 
    FROM
        TB_CourseClass 
    WHERE
        CourseClassID = '${req.query.CourseClassID}'
    `
    execute(req, res, sql_statement, 1)
});